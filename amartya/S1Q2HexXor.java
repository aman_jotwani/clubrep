import java.io.*;
class S1Q2HexXor
{
    public static void main() throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String num1 = br.readLine();
        String num2 = br.readLine();
        int[] numarr1 = new int[num1.length()], numarr2 = new int[num2.length()];
        for(int i = 0; i < num1.length(); i++)
        {
            numarr1[i] = Character.digit(num1.charAt(i),16);
            numarr2[i] = Character.digit(num2.charAt(i),16);
        }
        int[] numarr3 = new int[num1.length()];
        for(int i = 0; i < numarr1.length; i++)
            numarr3[i] = numarr1[i] ^ numarr2[i];
        String output = "";
        for(int i = 0; i < numarr3.length; i++)
        {
            output += (numarr3[i] <= 9)? (char)('0' + numarr3[i]):(char)('a' - 10 + numarr3[i]);
        }
        System.out.println(output);
    }
}