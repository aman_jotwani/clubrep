import java.io.*;
class S1Q3Decrypt
{
    public static void main() throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s = br.readLine();
        int[] nums = new int[s.length()/2];
        int[][] numtry = new int[26][s.length()/2];
        int[] scores = new int[26];
        int i;
        for(i = 0; i < 26; i++)
            scores[i] = 0;
        if(s.length()%2!=0)
            s = "0"+s;
        for(i = 0; i < s.length() - 1;i+=2)
        {
            String temp = s.substring(i, i+2);
            nums[i/2] = Integer.parseInt(temp,16);
        }
        for(int c = 65; c <= 90; c++)
        {
            for(i = 0; i < nums.length; i++)
            {
                numtry[c-65][i] = nums[i] ^ c;
                scores[c-65]+=getscore(numtry[c-65][i]);
            }
        }
        int[] maxstr = new int[nums.length];
        char maxChar = 'A';
        int maxScr = 0;
        for(i = 0; i < 26; i++)
        {
            if(scores[i] > maxScr)
            {
                maxChar = (char) (i + 65);
                maxScr = scores[i];
                maxstr = numtry[i];
            }
        }
        System.out.println("Character: "+maxChar+"\nScore: "+maxScr+"\nString: ");
        for(i = 0; i < maxstr.length; i++)
            System.out.print((char)maxstr[i]);
    }
    static int getscore(int n)
    {
        char temp = (char)n;
        if(Character.isUpperCase(temp)) temp = Character.toLowerCase(temp);
        if(!Character.isLetter(temp))return -1;
        return (26-"etaoinsrhldcumfpgwybvkxjqz".indexOf(temp));
    }
}