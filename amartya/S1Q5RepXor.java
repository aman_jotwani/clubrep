import java.io.*;
class S1Q5RepXor
{
    public static void main() throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input = br.readLine();
        String key = "ICE";
        int[] charArr = new int[input.length()]; //contains the characters in string's ascii values
        for(int i = 0; i < input.length(); i++)
        {
            charArr[i] = (int)input.charAt(i);
        }
        int temp;
        String output = "";
        for(int i = 0; i < charArr.length; i++)
        {
            temp = charArr[i] ^ (int)key.charAt(i%3);
            int up = temp / 16; // upper nibble
            int down = temp % 16; //lower nibble
            output += numtohex(up) + "" + numtohex(down);
        }
        System.out.println(output);
    }
    static char numtohex(int n)
    {
        return (n <= 9)? (char)('0' + n):(char)('a' - 10 + n);
    }
}