public class Stack {
   private int maxStack;
   private int emptyStack;
   private int top;
   private int[] items;
   public Stack(int size) {
      maxStack= size;
      emptyStack = -1;
      top = emptyStack;
      items = new int[maxStack];
   }

   public void push(int c) {
      items[++top] = c;
   }

   public int pop() {
      return items[top--];
   }

   public boolean full()  {
      return top + 1 == maxStack;
   }

   public boolean empty()  {
      return top == emptyStack;
   }
}
