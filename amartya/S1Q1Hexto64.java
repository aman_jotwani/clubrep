import java.io.*;
class S1Q1Hexto64
{
    public static void main()throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Stack hexinp = new Stack(100);
        String base64op = "";
        String base64lookup = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        System.out.println("Input:");
        String inp = br.readLine();
        for(int i = 0; i < inp.length(); i++)
        {
            char c = inp.charAt(i);
            hexinp.push(Character.digit(c,16));
        }
        while(!hexinp.empty())
        {
            int dig1 = hexinp.pop();
            int dig2 = hexinp.empty()?0:hexinp.pop();
            int dig3 = hexinp.empty()?0:hexinp.pop();
            int hexnum = 256 * dig3 + 16 * dig2 + dig1;
            //last digit
            int lastdig = hexnum & 63; // 03F or 000000 111111
            char lastchar = base64lookup.charAt(lastdig);
            //first digit
            int firstdigit = hexnum & 4032; //FC0 or 111111 000000
            firstdigit = firstdigit >> 6;
            char firstchar = base64lookup.charAt(firstdigit);
            base64op = firstchar + "" + lastchar + base64op;
        }
        System.out.println(base64op);
    }
}