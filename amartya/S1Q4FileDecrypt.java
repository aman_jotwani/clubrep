import java.io.*;
class S1Q4FileDecrypt
{
    public static void main() throws IOException
    {   
        //declarations
        String fileName = "S1Q4QText.txt";
        String line = null; //this variable will store a line from the file
        BufferedReader br = new BufferedReader(new FileReader(fileName)); //file stream
        
        int ln = 0;//stores the line number the program is currently reading
        int maxln = 0; //stores the line number which might be the answer
        int[] maxstr = new int[60]; //stores the plaintext i.e. the answer
        char maxChar = 'A'; //stores the cipher key
        int maxScr = 0; //stores the score of the plaintext
        
        String keys = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"; //list of possible cipher keys
        do //reads the file
        {
            ln++;
            line = br.readLine();//reads a line from file
            if(line == null) break;
            int[] ciph = new int[line.length()/2]; //stores the ciphertext char by char in int array
            int[][] plain = new int[keys.length()][line.length()/2]; //stores result of every XOR operation of every key value
            int[] scores = new int[keys.length()];//stores the score of every converted piece of text with every key
            int i;
            
            for(i = 0; i < 26; i++)
                scores[i] = 0; //sets scores to zero
            if(line.length()%2!=0)
                line = "0"+line; //pad line if odd number of symbols
                
            for(i = 0; i < line.length() - 1;i+=2)
            {
                String temp = line.substring(i, i+2);//extract hex numbers i.e. two chars from input ciphertext at a time
                ciph[i/2] = Integer.parseInt(temp,16);//convert to hex number
            }
            
            for(int c = 0; c < keys.length(); c++)//XOR the ciphertext with every key
            {
                for(i = 0; i < ciph.length; i++)
                {
                    plain[c][i] = ciph[i] ^ (int)keys.charAt(c);//XOR every char in text with key
                    scores[c]+=getscore(plain[c][i]);//character frequency score
                }
                for(i = 0; i < ciph.length - 1; i++)//digraph,double letter frequency score
                {
                    String temp = (char)plain[c][i] + "" + (char)plain[c][i+1];//make a digraph
                    scores[c]+=getDiScore(temp);
                }
            }
            
            for(i = 0; i < keys.length(); i++)//Search which key gave max score
            {
                if(scores[i] > maxScr)
                {
                    maxln = ln;
                    maxChar = keys.charAt(i);
                    maxScr = scores[i];
                    maxstr = plain[i];
                }
            }
        }while(line != null);
        System.out.println("Line: "+maxln+"\nCharacter: "+maxChar+"\nScore: "+maxScr+"\nString: ");
        for(int i = 0; i < maxstr.length; i++)
            System.out.print((char)maxstr[i]);
        br.close();
    }
    static int getscore(int n)
    {
        char temp = (char)n;
        if(!Character.isLetter(temp))return -5;
        if(Character.isUpperCase(temp)) temp = Character.toLowerCase(temp);
        return (26-"etaoinsrhldcumfpgwybvkxjqz".indexOf(temp));
    }
    static int getDiScore(String s)
    {
        int retScore = -1;
        String[] digraphs = {"th", "he", "an", "in", "er", "on", "re", "ed", "nd", "ha", "at", "en", "es", "of", "nt", "ea", "ti", "to", "io", "le", "is", "ou", "ar", "as", "de", "rt", "ve"};
        String[] doubles = {"ss", "ee", "tt", "ff", "ll", "mm", "oo"};
        for(int i = 0; i < digraphs.length; i++)
        {
            if(s.equalsIgnoreCase(digraphs[i]))
                retScore = (digraphs.length - i);
        }
        for(int i = 0; i < doubles.length; i++)
        {
            if(s.equalsIgnoreCase(doubles[i]))
                retScore = (doubles.length - i);
        }
        return retScore;
    }
}