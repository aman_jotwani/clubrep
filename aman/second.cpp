#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include <string.h>

/* NOTE:
   Usage: g++ -g -o second second.cpp

   The two strings might be in different files but here I assume
   there are separated by new line in one file.
   Steps:
   1) Divide data string and key into chunks of 4 bytes each. On my machine, I could take 8 bytes too since the machine word length is 64 bits.
   Its about filling the registers completely and then performing xor, which has a one to one translation to assembly. Such considerations seem
   to be good and efficient but I dont exactly know how much time they save.
   2) Convert both to hex, perform xor.
   
   TODO:
   Find out why freeing at the end causes the program to freeze.
   Understand the significance of padding here and how it produces weird bugs.
   
   Code organization is bad, I couldnt easily factor out fixed_xor for use in other
   programs. Solution - Either remove dividing into chunks altogether, or do that
   in fixed_xor so that copy-pasting your own code "just works".
 */

// NOTE:
// this function converts strings known to be ascii-encoded hex values into bytes
uint32_t Ascii_To_Hex(char* chunk)
{
    uint32_t rval = 0;
    uint8_t  shift = 0;
    
    for(int i = 7; i >= 0; i--)
    {
        if(isxdigit(chunk[i]) != 0)
        {
            // conversion using offsets
            // shift is to arrange the individually converted
            // hex digits into a 32 bit space.
            if(isdigit(chunk[i]))
                rval += (chunk[i] - 48) << (4*shift);
            else if(islower(chunk[i]))
                rval += (chunk[i] - 87) << (4*shift);
            else
                rval += (chunk[i] - 55) << (4*shift);
            shift += 1;
        }
    }
    return rval;
}

void Fixed_XOR(char* data, char* key, char* result_str)
{
    uint32_t data_chunk = Ascii_To_Hex(data);
    uint32_t key_chunk = Ascii_To_Hex(key);
    uint32_t result = data_chunk ^ key_chunk;
    sprintf(result_str, "%x", result);
}

int main(int argc, char** argv)
{
    FILE* input_stream;
    char* mem_chunk = NULL;
    char* plaintext = NULL;
    char* result_string = NULL;
    char* key = NULL;
    uint8_t num_padding = 0;
    
    input_stream = fopen("second_input.txt", "r");

    fseek(input_stream, 0, SEEK_END);
    long file_size = ftell(input_stream);
    rewind(input_stream);
    size_t buffer_size = ((file_size-4)/2);      // not counting the new line characters

    // padding calculation
    if(buffer_size % 8 != 0)
    {
        num_padding = 8 - (buffer_size % 8);
        buffer_size += num_padding;
    }
    size_t num_databytes = buffer_size - num_padding;
    
    mem_chunk = (char*)malloc(3*buffer_size);
    if(mem_chunk == NULL)
    {
        printf("Malloc failed.\n");
        return 0;
    }
    else
    {
        memset(mem_chunk, 0, sizeof(char));
        plaintext = mem_chunk;
        key = mem_chunk + buffer_size;
        result_string = mem_chunk + 2*buffer_size;
    }
    
    size_t bytes_read = fread(plaintext, sizeof(char), num_databytes, input_stream);
    if(bytes_read != num_databytes)
    {
        printf("fread failed to read first line of plaintext. Only read %d bytes.\n", bytes_read);
        free(mem_chunk);
        return 0;
    }
    else
    {    
        fseek(input_stream, 2, SEEK_CUR);         // to skip the new lines
        
        bytes_read = fread(key, sizeof(char), num_databytes, input_stream);
        if(bytes_read != num_databytes)
        {
            printf("fread failed to read second line of key. Only read %d bytes.\n", bytes_read);
            free(mem_chunk);
            return 0;
        }
        else
        {       
            char data_part[9] = "";
            char key_part[9] = "";
            data_part[8] = '\0';
            key_part[8] = '\0';

            char* temp = result_string;
            while(buffer_size > 0)
            {
                // dont need to manually set the padding bytes to 0 since strncpy takes care of that.
                strncpy(data_part, plaintext, 8);
                strncpy(key_part, key, 8);
                Fixed_XOR(data_part, key_part, temp);

                buffer_size -= 8;
                plaintext += 8;
                temp += 8;
                key += 8;
            }

            printf("%s\n", result_string);
            const char* known_output = "746865206b696420646f6e277420706c6179";
            if(strcmp(result_string, known_output) == 0)
                printf("Correct answer!\n");
            else
                printf("Wrong answer!\n");
        }
    }
    free(mem_chunk); // freeing memory right before you exit is actually a bad practice in real world programs.
    fclose(input_stream);
    return 0;
}
