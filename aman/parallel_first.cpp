#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>

// arguments to a threaded function have to be encapsulated into one. 
struct Ascii_Chunk
{
    char data[6];
    int  position;   // wrt parent
};

uint32_t ascii_to_hex(char* chunk)
{
    uint32_t rval = 0;
    uint8_t  shift = 0;
    
    for(int i = 5; i >= 0; i--)
    {
        if(isxdigit(chunk[i]) != 0)
        {
            // using offsets
            if(isdigit(chunk[i]))
                rval += (chunk[i] - 48) << (4*shift);
            else if(islower(chunk[i]))
                rval += (chunk[i] - 87) << (4*shift);
            else
                rval += (chunk[i] - 55) << (4*shift);
            shift += 1;
        }
    }
    return rval;
}

void* b64_convert(void* chunk)
{
    Ascii_Chunk* achunk = (Ascii_Chunk*) chunk;
    uint32_t hex_chunk = ascii_to_hex(achunk->data);
    // prepare for lookup 
    uint32_t first_chunk = (hex_chunk & (0xFC << 16)) >> 18;
    uint32_t second_chunk = (hex_chunk & (0xFC << 10)) >> 12;
    uint32_t third_chunk = (hex_chunk & (0xFC << 4)) >> 6;
    uint32_t fourth_chunk = (hex_chunk & (0x3F));
    

    // lookup and print
    char* lookup_table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    char* retstr = (char*)malloc(4);
    sprintf(retstr, "%c%c%c%c", lookup_table[first_chunk], lookup_table[second_chunk],
            lookup_table[third_chunk], lookup_table[fourth_chunk]);
    return retstr;
}


int main(void)
{
    char* ascii_data = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d\0";

    size_t input_len = strlen(ascii_data);
    int lencpy = input_len;
    pthread_t threads[input_len/6];
    int i = 0;
    Ascii_Chunk temp[input_len/6];
    
    while(input_len > 0)
    {
        strncpy(temp[i].data, ascii_data, 6);
        temp[i].position = 6*i;
        int rval = pthread_create(&threads[i], NULL, &b64_convert, (void*)(&temp[i]));
        if(rval != 0)
        {
            printf("Thread creation failed.\n");
            break;
        }
        else
            i += 1;

        ascii_data += 6;
        input_len -= 6;
        
    }

    void* res;
    for(int i = 0; i < lencpy/6; i++)
    {
        if(pthread_join(threads[i], &res) != 0)
        {
            printf("thread join failed.\n");
            break;
        }
        else
        {
            printf("%s", (char*)res);
            free(res);
        }
    }
    printf("\n");
    
    return 0;
}
