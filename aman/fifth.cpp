#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
/* NOTE:
   Usage: g++ -g -o fifth fifth.cpp

   Implementing repeating key XOR.
 */

void Repeat_XOR_Encrypt(const char* source_text, const size_t source_len, const char* key, char* result)
{
    size_t key_len = strlen(key);
    uint8_t k = 0;
    uint8_t temp = 0;
    
    for(size_t i = 0; i < source_len; i++)
    {
        if(k == key_len)
            k = 0;
        
        temp = ((uint8_t)key[k]) ^ ((uint8_t)source_text[i]);
        sprintf(result, "%x", temp);
        printf("%s", result);
        result += 2;
        k += 1;
    }
}

int main(int argc, char** argv)
{
    static const char* key = "ICE";
    static const char* teststr = "Burning \'em, if you ain\'t quick and nimble\nI go crazy when I hear a cymbal";
    static const size_t source_len = strlen(teststr);
    char* ciphertext = (char*)malloc(2*source_len+1);
    char* temp = ciphertext;
    memset(ciphertext, 0, sizeof(char));
    ciphertext[2*source_len] = '\0';
        
    Repeat_XOR_Encrypt(teststr, source_len, key, temp);
    printf("%s\n", ciphertext);
    return 0;
}
