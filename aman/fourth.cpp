#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>

/*
  NOTE: Usage: g++ -g -o fourth fourth.cpp
  
  Performs bruteforce alphabetic(upper and lowercase) and numerical key-search.

  Input contained a trick input where the size of the string was not 60 characters,
  fread was reading the bytes from the next string instead of detecting the newline.
  fgets used instead, and padding calculated accordingly to make sure all input is
  60 characters before decryption.
 */
uint8_t Ascii_To_Hex(const char* chunk)
{
    uint8_t rval = 0;
    uint8_t  shift = 0;
    
    for(int i = 1; i >= 0; i--)
    {
        if(isxdigit(chunk[i]) != 0)
        {
            // conversion using offsets
            if(isdigit(chunk[i]))
                rval += (chunk[i] - 48) << (4*shift);
            else if(islower(chunk[i]))
                rval += (chunk[i] - 87) << (4*shift);
            else
                rval += (chunk[i] - 55) << (4*shift);
            shift += 1;
        }
    }
    return rval;   
}

int Plaintext_Judge(char* plaintext, size_t len)
{
    int score = 0;
    static const char* common[13] = {"of", "in", "so", "if", "to", "it", "at", "as", "on", "is", "the", "and", "for"};
    // more weightage to containing common pairs of letters.
    for(int i = 0; i < 13; i++)
    {
        if(strstr(plaintext, common[i]) != NULL)
            score += 5;
    }
    
    // -1 for everytime it contains something other than alphanumeric, space or punctuation
    for(int i = 0; i < len - 1; i++)
    {
        if(isalnum(plaintext[i]) == 0 &&
           isspace(plaintext[i]) == 0 &&
           ispunct(plaintext[i]) == 0)
        {
            score -= 1;
        }
    }
    // +1 for each -- first letter capital, ends with a fullstop.
    if(isupper(plaintext[0]) != 0)
        score += 1;
    if(plaintext[len-1] == '.')
        score += 1;

    return score;
}

void Bruteforce_Search(uint8_t key_begin, uint8_t key_end,
                       char* ciphertext, size_t ciphertext_size,
                       char* plaintext, size_t text_size,
                       int* max_score)
{
    size_t temp = ciphertext_size;
    uint8_t key =  key_begin;
    char* str_counter = ciphertext;
    size_t i = 0;
    int score = 0;
    
    while(key != key_end)
    {
        while(temp != 0)
        {
            plaintext[i] = (char)(Ascii_To_Hex(str_counter)^key);

            i += 1;
            str_counter += 2;
            temp -= 2;
        }

        score = Plaintext_Judge(plaintext, text_size);
        if(score > *max_score)
        {
            *max_score = score;
            printf("score - %d\tcurrent solution -- %s\n\n", score, plaintext);
        }
        
        key += 1;
        temp = ciphertext_size;
        str_counter = ciphertext;
        i = 0;
    }
}

int Decrypt(char* ciphertext, int max_score)
{
    static const size_t ciphertext_size = 60;
    static const size_t text_size = 30;
    static char plaintext[text_size+1];
    plaintext[text_size] = '\0';
    Bruteforce_Search(65, 90, ciphertext, ciphertext_size, plaintext, text_size, &max_score);
    Bruteforce_Search(97, 122, ciphertext, ciphertext_size, plaintext, text_size, &max_score);
    Bruteforce_Search(49, 57, ciphertext, ciphertext_size, plaintext, text_size, &max_score);
    
    return max_score;
}

int main(int argc, char** argv)
{
    FILE* input_stream;
    input_stream = fopen("four_input.txt", "r");
    static const size_t buffer_size = 60;
    char ciphertext[buffer_size+1];
    
    int max_score = -100;
    int score = 0;
    uint32_t count = 1;
    size_t bytes_read;
    bool done = false;
    
    while(!done)
    {
        // fgets counts new line as a character and automatically null terminates
        // the destination string
        fgets(ciphertext, buffer_size+1, input_stream);

        if(feof(input_stream))
            done = true;
        else
        {            
            bytes_read = strlen(ciphertext);
            fseek(input_stream, 2, SEEK_CUR); // to skip the new line.

            if(bytes_read != buffer_size)
            {
                // pad with zeroes
                // fgets counts new line as a byte read, so add 1 to padding
                // to compensate
                uint32_t padding = buffer_size - bytes_read + 1;
                for(size_t i = buffer_size - padding; i < buffer_size; i++)
                    ciphertext[i] = '0';
                fseek(input_stream, -1*padding, SEEK_CUR);
            }
            
            score = Decrypt(ciphertext, max_score);
            if(score > max_score)
            {
                max_score = score;
                printf("score -- %d count -- %d\n", score, count);
            }
            count += 1;
        }
    }
}
