#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <assert.h>

/* USAGE: g++ -g -o sixth sixth.cpp
   TODO: statistical approach, research vigenere
   TODO: Time this program, make a parallel version and time it too. Compare.
   TODO: Write tests to save time on testing already coded functions.
 */
#define NOT_HEX 255

uint8_t Single_Ascii_To_Hex(char c)
{
    uint8_t rval = NOT_HEX;
    if(isxdigit(c) != 0)
    {
        if(isdigit(c))
            rval = (c - 48);
        else if(islower(c))
            rval = (c - 87);
        else
            rval = (c - 55);
    }
    return rval;
}

/*NOTE:
  this function converts strings known to be 8 ascii-encoded hex values into 4 bytes.
  depends on ctype.h and stdint.h
*/
uint32_t Ascii_To_Hex(char* chunk)
{
    uint32_t rval = 0;
    uint8_t  shift = 0;
    uint8_t str_elem = 0;

    for(int i = 7; i >= 0; i--)
    {
        str_elem = Single_Ascii_To_Hex(chunk[i]);
        if(str_elem != NOT_HEX)
        {
            rval += ((uint32_t)str_elem) << (4*shift);
            shift += 1;
        }
    }
    return rval;
}

void B64_To_Hex(char* b64_string, char* hex_string, int cipher_len)
{
    // divide into chunks of 4 b64 digits
    // convert to hex by looking up byte-representation
    // and arranging them in a uint32_t in a compact way to produce 3 bytes
    // snprintf them into hex_string, why?!
    static const char* lookup_table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    uint32_t first_part, second_part, third_part, fourth_part, flag;
    char temp[5] = "";
    temp[4] = '\0';
    char result[6] = "";
    char* counter = NULL;
    char* dest_counter = hex_string;
    size_t chunk_len = 0;
    uint32_t chunk = 0;
    uint32_t num_padding = 0;
    
    while(cipher_len > 0)
    {
        strncpy(temp, b64_string, 4);
        for(int i = 0; i < 64; i++)
        {
            if(temp[0] == lookup_table[i])
            {
                first_part = i;
                flag |= 1;
            }
            if(temp[1] == lookup_table[i])
            {
                second_part = i;
                flag |= 2;
            }
            if(temp[2] == lookup_table[i])
            {
                third_part = i;
                flag |= 4;
            }
            if(temp[3] == lookup_table[i])
            {
                fourth_part = i;
                flag |= 8;
            }
            if(flag == 15)
                break;
        }

        chunk |= (first_part << 18);
        chunk |= (second_part << 12);
        chunk |= (third_part << 6);
        chunk |= fourth_part;
        
        sprintf(result, "%x", chunk);
        chunk_len = strlen(result);
        // padding
        if(chunk_len != 6)
        {
            counter = result;
            num_padding = 6 - chunk_len;
            for(int i = 0; i < num_padding; i++)
                counter[i] = '0';
            counter += num_padding;
            sprintf(counter, "%x", chunk);
            strncpy(dest_counter, result, 6);
        }
        else
        {
            strncpy(dest_counter, result, 6);
        }
         
        cipher_len -= 4;
        b64_string += 4;
        dest_counter += 6;
        chunk = 0;
        first_part = 0;
        second_part = 0;
        third_part = 0;
        fourth_part = 0;
        flag = 0;
    }
}

uint32_t Popcount(uint32_t num)
{
    uint32_t rval = 0;
    uint32_t remainder = 0;
    while(num != 0)
    {
        remainder = num % 2;
        if(remainder == 1)
            rval += 1;
        num /= 2;
    }
    return rval;
}

uint32_t Get_Hamming_Dist(const char* first, const char* second)
{
    size_t first_len = strlen(first);
    size_t second_len = strlen(second);
    assert(first_len == second_len);
    uint32_t x = 0;
    uint32_t y = 0;
    uint32_t result = 0;
    uint32_t rval = 0;

    for(size_t i = 0; i < first_len; i++)
    {
        // NOTE: this is a bug for ascii-encoded hex values, but works for ascii-encoded plaintext
        // x = (uint32_t)first[i];
        // y = (uint32_t)second[i];
        x = (uint32_t)Single_Ascii_To_Hex(first[i]);
        y = (uint32_t)Single_Ascii_To_Hex(second[i]);
        result = x ^ y;
        rval += Popcount(result);
    }
    return rval;
}

uint32_t Guess_Keysize(char* data)
{
    const size_t max_keysize = strlen(data)/2;
    
    // NOTE: not good practice. but Get_Hamming_Dist uses strlen, which might mess up
    // one allocation. possible solution could be to pass in the size as an arg.
    char first[46] = "";
    first[max_keysize] = '\0';
    char second[46] = "";
    second[max_keysize] = '\0';
    float min = 1000.0f;
    float normalized = 0.0f;
    uint32_t temp_hamming = 0;
    uint32_t curr_min_hamming = 0;
    
    for(size_t i = 2; i <= max_keysize; i++)
    {
        strncpy(first, data, i);
        strncpy(second, (data+i), i);
        temp_hamming = Get_Hamming_Dist(first, second);
        normalized = ((float)temp_hamming)/i;
        if(normalized < min)
        {
            min = normalized;
            curr_min_hamming = i;
        }
    }
    return curr_min_hamming;
}

void Decrypt(uint32_t keysize)
{
    // transpose bytes
    // perform single-byte xor
    // if any byte is not alphanumeric, punctual or space, discard that key
    // join the bytes back and score the plaintext using plaintext_judge
}

int main(int argc, char** argv)
{
    FILE* input_stream;
    static const size_t cipher_size = 60;
    size_t bytes_read = 0;
    char ciphertext[cipher_size+1] = "";
    char hex_string[(cipher_size/4)*6 + 1] = "";
    bool done = false;
    uint32_t keysize = 0;
    uint32_t min_keysize = 1000;
    input_stream = fopen("sixth_input.txt", "r");
    uint32_t count = 0;
    
    while(!done)
    {
        fgets(ciphertext, cipher_size+1, input_stream);
        if(feof(input_stream))
            done = true;
        else
        {
            count += 1;            
            bytes_read = strlen(ciphertext);
            fseek(input_stream, 2, SEEK_CUR);
            
            if(bytes_read != cipher_size)
            {
                // pad with zeroes
                // fgets counts new line as a byte read, so add 1 to padding
                // to compensate
                uint32_t padding = cipher_size - bytes_read + 1;
                for(size_t i = cipher_size - padding; i < cipher_size; i++)
                    ciphertext[i] = 'A'; // 0 in base64 will be 'A' and not '0'
                // NOTE: This is only valid when padding is required in the middle of a file,
                // when done towards the end, this blocks the eof call somehow
                // fseek(input_stream, -1*padding, SEEK_CUR);
            }
            B64_To_Hex(ciphertext, hex_string, cipher_size);
            keysize = Guess_Keysize(hex_string);
            if(keysize < min_keysize)
            {
                min_keysize = keysize;
            }
            Decrypt(min_keysize);
        }
    }
    return 0;
}
