#include <stdio.h>  // printf
#include <stdlib.h> // malloc, free
#include <ctype.h>  // isxdigit, isdigit
#include <stdint.h> // types
#include <string.h> // strlen

/* NOTE: 
   Usage: g++ -g -o third third.cpp
   
   Performs an alphanumeric search for single-byte key.
*/

uint8_t Ascii_To_Hex(const char* chunk)
{
    uint8_t rval = 0;
    uint8_t  shift = 0;
    
    for(int i = 1; i >= 0; i--)
    {
        if(isxdigit(chunk[i]) != 0)
        {
            // conversion using offsets
            // shift is to arrange the individually converted
            // hex digits into a 32 bit space.
            if(isdigit(chunk[i]))
                rval += (chunk[i] - 48) << (4*shift);
            else if(islower(chunk[i]))
                rval += (chunk[i] - 87) << (4*shift);
            else
                rval += (chunk[i] - 55) << (4*shift);
            shift += 1;
        }
    }
    return rval;   
}

// NOTE: more or less random scoring scheme for now. Itll work till the day it wont.
int Plaintext_Judge(char* plaintext, size_t len)
{
    int score = 0;
    static const char* common[13] = {"of", "in", "so", "if", "to", "it", "at", "as", "on", "is", "the", "and", "for"};
    // more weightage to containing common pairs of letters.
    for(int i = 0; i < 13; i++)
    {
        if(strstr(plaintext, common[i]) != NULL)
            score += 5;
    }
    
    // -1 for everytime it contains something other than alphanumeric, space or punctuation
    for(int i = 0; i < len - 1; i++)
    {
        if(isalnum(plaintext[i]) == 0 &&
           isspace(plaintext[i]) == 0 &&
           ispunct(plaintext[i]) == 0)
        {
            score -= 1;
        }
    }
    // +1 for each -- first letter capital, ends with a fullstop.
    if(isupper(plaintext[0]) != 0)
        score += 1;
    if(plaintext[len-1] == '.')
        score += 1;

    return score;
}

// NOTE: Sequentially checks for keys starting with key_begin till key_end.
void Bruteforce_Search(uint8_t key_begin, uint8_t key_end,
                       char* ciphertext, size_t ciphertext_size,
                       char* plaintext, size_t text_size,
                       int* max_score)
{
    size_t temp = ciphertext_size;
    uint8_t key =  key_begin;
    char* str_counter = ciphertext;
    size_t i = 0;
    int score = 0;
    
    while(key != key_end)
    {
        while(temp != 0)
        {
            plaintext[i] = (char)(Ascii_To_Hex(str_counter)^key);

            i += 1;
            str_counter += 2;
            temp -= 2;
        }

        score = Plaintext_Judge(plaintext, text_size);
        if(score > *max_score)
        {
            *max_score = score;
            printf("score - %d\tcurrent solution -- %s\n", score, plaintext);
        }
        
        key += 1;
        temp = ciphertext_size;
        str_counter = ciphertext;
        i = 0;
    }
}

int main(int argc, char** argv)
{
    static char* ciphertext = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736";
    static const size_t ciphertext_size = strlen(ciphertext);
    static const size_t text_size = ciphertext_size/2;
    char* plaintext = (char*)malloc(text_size);
    int max_score = -100;
    
    Bruteforce_Search(65, 90, ciphertext, ciphertext_size, plaintext, text_size, &max_score); // uppercase alphabets
    Bruteforce_Search(97, 122, ciphertext, ciphertext_size, plaintext, text_size, &max_score); // lowercase alphabets
    Bruteforce_Search(49, 57, ciphertext, ciphertext_size, plaintext, text_size, &max_score); // numerals
    
    free(plaintext); // bad practice
    return 0;
}


