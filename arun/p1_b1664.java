import java.io.*;
import java.util.ArrayList;
import java.lang.Object;

class b1664copy {
    public static void main(String[] args)throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String inp = br.readLine(), hexa, qhex = new String();
        int hexdec, decqhex;
        char b64;
        int pow2;
        qhex = "";
        ArrayList<Integer> binqueue = new ArrayList<Integer>();
       
        if(inp.length() % 3 == 1)               //Padding the input with extra 0s so that the 
            hexa = "00" + inp;                  //input in binary form is divisible by 6
        else if(inp.length() % 3 == 2)
            hexa = "0" + inp;
        else
            hexa = inp;
       
        for(char temp:hexa.toCharArray()){
            
            if(temp >= '0' && temp <= '9')
                hexdec = (temp - 48);
            else
                hexdec = (temp - 55);
                
            for(int i = 0; i < 4; ++i){
                binqueue.add((hexdec & 8)>> 3);
                hexdec = hexdec << 1;
            }
            
            if(binqueue.size() >= 6){
                pow2 = 32;
                decqhex = 0;
                for(int i = 0; i < 6; ++i){
                    decqhex += pow2 * binqueue.remove(0);
                    pow2 /= 2;
                }
                if(decqhex >= 0 && decqhex <= 25)
                    b64 = (char)(decqhex+ 65);
                else if(decqhex >=26 && decqhex <= 51)
                    b64 = (char)(decqhex + 71);
                else if(decqhex >=52 && decqhex <= 61)
                    b64 = (char)(decqhex - 4);
                else if(decqhex == 62)
                    b64 = '+';
                else 
                    b64 = '/';
                qhex = qhex + b64;
            }
        }
        System.out.println(qhex);
    }
}